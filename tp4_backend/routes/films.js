const express = require('express');
const _ = require('lodash');
const router = express.Router();
const API_FILM = require('../public/javascripts/API_FILMS');
const axios = require('axios');
/*OMDb API: http://www.omdbapi.com/?i=tt3896198&apikey=115cccc5*/

let films= [{
        id: "",
        title: "",
        yearOfRelease: 0,
        duration: 0,
        actors: ["", ""],
        poster: "",
        boxOffice: 0,
        rottenTomatoesScore: ""
}];

/* GET films listing. */
router.get('/', (req, res) => {
    // Get List of user and return JSON
    res.status(200).json({ films });
});

/* GET one film. */
router.get('/:id', (req, res) => {
    const { id } = req.params;
    // Find user in DB
    const film = _.find(films, ["id", id]);
    // Return user
    res.status(200).json({
        message: 'Film found!',
        film
    });
});

/* PUT new film. */
router.put('/:title', (req, res) => {
    // Get the data from request
    const  titreFilm  = req.params.title;
    console.log({titreFilm});
    // Create new unique id
    //const id = _.uniqueId();

    //récupéré le titre du film dans la requete
    // faire l'appel à l'API
    //isoler les données que l'on veut
    // push dans notre DB
    const apiFilm = new API_FILM();
    apiFilm
        .fetchFilm(titreFilm)
        .then(function(response)
     {
        const data = response.data;
        console.log("data", data);
        // On récupère l'information principal
        const id = data.imdbID;
        const title = data.Title;
        const yearOfRelease = data.Released;
        const duration = data.Runtime;
        const actors = data.Actors;
        const poster = data.Poster;
        const boxOffice = data.BoxOffice;
        const rottenTomatoesScore = data.Ratings[1];

        films.push({ id, title, yearOfRelease, duration, actors, poster, boxOffice, rottenTomatoesScore});
        // Return message
        res.status(200).json({
            message: `Just added ${id}`,
            titre: { title, id }
        });
    });

    // Insert it in array (normaly with connect the data with the database)

});

/* DELETE user. */
router.delete('/:id', (req, res) => {
    // Get the :id of the user we want to delete from the params of the request
    const { id } = req.params;

    // Remove from "DB"
    _.remove(films, ["id", id]);

    // Return message
    res.json({
        message: `Just removed ${id}`
    });
});

/* UPDATE user. */
router.post('/:id', (req, res) => {
    // Get the :id of the user we want to update from the params of the request
    const { id } = req.params;
    // Get the new data of the film we want to update from the body of the request
    const { title } = req.body;
    // Find in DB
    const filmToUpdate = _.find(films, ["id", id]);
    // Update data with new data
    filmToUpdate.title = title;

    // Return message
    res.json({
        message: `Just updated ${id} with ${title}`
    });
});

module.exports = router;